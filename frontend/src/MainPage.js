function MainPage() {
    return (
      <div>
        <h1>WARDROBIFY!</h1>
        <div>
          <p>
            Need to keep track of your shoes and hats? We have
            the solution for you!
          </p>
        </div>
      </div>
    );
  }
  
  export default MainPage;