import { BrowserRouter, Routes, Route } from 'react-router-dom';




import MainPage from './MainPage';
import Nav from './Nav';



import VisitList from './visits/VisitList';
import VisitForm from './visits/VisitForm'
import VisitDetail from './visits/VisitDetail';



function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="visits">
            <Route index element={<VisitList />} />
            <Route path="new" element={<VisitForm />} />
            <Route path=":id" element={< VisitDetail />} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
