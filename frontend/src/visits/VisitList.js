import { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import './visits.css'


function VisitList(props) {

    const [visits, setVisits] = useState([]);

    async function fetchVisits() {
        const response = await fetch('http://localhost:8000/visits/')

        if (response.ok){
            const parsedJson = await response.json();
            setVisits(parsedJson.visits);
        }
    }


    useEffect(() => {
        fetchVisits();
    }, []);


    return (
        visits.map(visit => {
        return (
            <div class="container">
                <img src="https://cdn.discordapp.com/attachments/465559014395084801/1138885546177998928/note.png" alt="pink sticky note" width="300px" />
                <div className="top-left"><h2>{visit.first_initial}. {visit.last_name}, {visit.state}</h2></div>
                <div className="centered">{visit.message}</div>
                <div className="bottom-centered">
                    <Link to={`/visits/${visit.id}`}>
                        <button className="button-small" type="button">
                            Full Visit...
                        </button>
                    </Link>
                </div>
            </div>
        );
        })
    );
};

    export default VisitList;