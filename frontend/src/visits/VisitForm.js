import React, {useState} from 'react';

function VisitorForm () {

  const [firstInitial, setFirstInitial] = useState("")
  const [lastName, setLastName] = useState("")
  const [message, setMessage] = useState("")
  const [state, setState] = useState("")



  const handleSubmit = async (event) => {
    event.preventDefault();

    const data = {};
    data.first_initial = firstInitial;
    data.last_name = lastName;
    data.message = message;
    data.state = state;


    const url = `http://localhost:8000/visits/`;

    const fetchConfig = {
      credentials: 'include',
      method: "post",
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const response = await fetch(url, fetchConfig);

    if (response.ok) {
      setFirstInitial("");
      setLastName("");
      setMessage("");
      setState("");
    }
  }

  const handleFirstInitialChange = (e) => {
    setFirstInitial(e.target.value);
  }

  const handleLastNameChange = (e) => {
    setLastName(e.target.value);
  }


  const handleMessageChange = (e) => {
    setMessage(e.target.value);
  }

  const handleStateChange = (e) => {
    setState(e.target.value)
  }

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Submit an Anonymous Visitor Message!</h1>
          <form onSubmit={handleSubmit} id="create-visit-form">
            <div className="form-floating mb-3">
              <input onChange={handleFirstInitialChange} value={firstInitial} placeholder="FIRST INITIAL" required name="firstInitial" type="text" id="firstInitial" className="form-control" />
              <label htmlFor="firstInitial"></label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleLastNameChange} value={lastName} placeholder="LAST NAME" required name="lastName" type="text" id="lastName" className="form-control" />
              <label htmlFor="lastName"></label>
            </div>
            <div className="form-floating mb-3">
              <textarea rows={10} columns={7} onChange={handleMessageChange} maxLength={150} value={message} placeholder="Type your anonymous message..." required name="message" type="textarea" id="message" className="form-control" />
              <label htmlFor="message"></label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleStateChange} value={state} placeholder="STATE ABBR." required name="state" type="text" id="state" className="form-control" />
              <label htmlFor="state"></label>
            </div>
            <button className="btn btn-primary">Send Visit!</button>
          </form>
        </div>
      </div>
    </div>
  );
}

export default VisitorForm;