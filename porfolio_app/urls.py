from django.urls import path
from .views import visit_detail, visit_list

urlpatterns = [
    path("", visit_list, name="visit_list"),
    path("<int:id>/", visit_detail, name="visit_detail"),
]