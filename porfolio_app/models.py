from django.db import models

# Create your models here.
class Visit(models.Model):
    last_name = models.CharField(max_length=100)
    first_initial = models.CharField(max_length=1)
    state = models.CharField(max_length=2)
    message = models.CharField(max_length=200)
    datetime = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.last_name