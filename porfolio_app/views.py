from django.shortcuts import render
from django.views.decorators.http import require_http_methods
from .models import Visit
from django.http import JsonResponse
import json
from .encoder import VisitDetailEncoder, VisitListEncoder





############### VISIT LIST #################

@require_http_methods(["GET", "POST"])
def visit_list(request):

    ### show ALL visits (GET) ###

    if request.method == "GET":
        visits = Visit.objects.all()
        return JsonResponse(
            {"visits": visits},
            encoder = VisitListEncoder
        )
    
    ### create a NEW visit (POST) ###

    else:
        content = json.loads(request.body)
        visit = Visit.objects.create(**content)
        return JsonResponse(
            visit,
            encoder=VisitDetailEncoder,
            safe=False
        )







############### VISIT DETAIL #################


@require_http_methods(["DELETE", "GET"])
def visit_detail(request, id):

    ### show ONE visit by ID (GET) ###

    if request.method == "GET":
        try:
            visit = Visit.objects.get(id=id)
            return JsonResponse(
                visit,
                encoder=VisitDetailEncoder,
                safe=False
            )
        except Visit.DoesNotExist:
            response = JsonResponse({"message": "This visit does not exist! Maybe it was deleted...?"})
            response.status_code = 404
            return response
        
    ### delete ONE visit by ID (DELETE) ###

    else: 
        try:
            visit = Visit.objects.get(id=id)
            visit.delete()
            return JsonResponse(
                visit,
                encoder=VisitDetailEncoder,
                safe=False,
            )
        except Visit.DoesNotExist:
            return JsonResponse({"message": "This visit does not exist!"})

