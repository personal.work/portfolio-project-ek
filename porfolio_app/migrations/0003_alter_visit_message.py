# Generated by Django 4.2.4 on 2023-08-22 16:32

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('porfolio_app', '0002_alter_visit_message'),
    ]

    operations = [
        migrations.AlterField(
            model_name='visit',
            name='message',
            field=models.CharField(max_length=150),
        ),
    ]
