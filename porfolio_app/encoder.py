from common.json import ModelEncoder
from .models import Visit

class VisitListEncoder(ModelEncoder):
    model = Visit
    properties = [
        "id",
        "last_name",
        "first_initial",
        "state",
        "message",
    ]

class VisitDetailEncoder(ModelEncoder):
    model = Visit
    properties = [
        "id",
        "last_name",
        "first_initial",
        "state",
        "message",
        "datetime",
    ]